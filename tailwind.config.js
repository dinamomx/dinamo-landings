module.exports = {
  theme: {
    extend: {
      fontFamily: {
        sans: ['Roboto Condensed', 'Helvetica', 'Calibri', 'sans-serif']
      }
    }
  },
  variants: {},
  plugins: [require('@tailwindcss/custom-forms')]
};
